// import {URL} from "url";
$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results[1] || 0;
};


// var current_step = 1;
//
// window.onpopstate = function(event) {
//     if(current_step - event.state.step > 0) {
//         console.log("上一頁");
//     }else{
//         console.log("下一頁");
//     }
// };
//
// function show_step1( ) {
//     current_step = 1;
//     history.pushState({step: 1}, "第一步", "/step/1");
// }
//
// function show_step2( ) {
//     current_step = 2;
//     history.pushState({step: 2}, "第二步", "/step/2");
// }
//
// history.pushState({step: 1}, "第一步", "/FaceMaskShop");
//
// window.onpopstate = function(event) {
//
//     console.log(event.state);
//
// };
// var stateObj = { foo: "bar" };
// history.pushState(stateObj, "page 1", "FaceMaskShop");
// var current_step = 1;
// window.onpopstate = function(event) {
//
//     if(current_step - event.state.step > 0) {
//         console.log("上一頁");
//     }else{
//         console.log("下一頁");
//     }
//
// };
//
// window.addEventListener('popstate', function(evt){
//
//     var state = evt.state;
//
//    console.log(state);
//
// }, false);


//------ history.pushState（） 上一頁下一頁作法(未完成測試)----------//
// window.history.pushState("http://example.ca", "Sample Title", "/example/path.html");
// window.onpopstate = function(event) {
//     alert(`location: ${document.location}, state: ${JSON.stringify(event.state)}`);
//     // history.back();
// };
// history.pushState({page: 1}, "title 1", "#list");
// history.pushState({page: 2}, "title 2", "#list_detail");
// history.replaceState({page: 3}, "title 3", "?page=3");
//--------------------------------------------------------------------//


// let githubURL = new URL(location.href);
// let params = githubURL.searchParams;
// var token = params.get('token');
// var token = $.urlParam('type');

var token_fun = (function () {
    var url;

    function set(token) {
        url = token;
    }

    function get() {
        return url;
    }

    return {
        set: set,
        get: get
    }
})();
// token_fun.set(token);
// alert(token_fun.get());


//---- 中間件組件 ----//
var middleWare = (function () {
    function checkMember(mem) {
        // alert(mem);
    }

    //移除模板
    function removeTmp() {
        //移除模板
        $(".tmp").remove();
    }

    return {
        checkMember: checkMember,
        removeTmp: removeTmp

    }
})();


//---- 路由器組件 ----//
var nav = (function () {
    var $viewStage = $('#viewStage');

    function showPage(token, parm) {
        //移除模板
        middleWare.removeTmp();
        //檢查會員權限
        middleWare.checkMember("andy");
        // $(window).off("hashchange");


        //執行路由
        // router.router(window.location.hash);
        router.router(token);
        return false;


    }

    return {
        showPage: showPage
    }

})();


//---- 讀取所有套件 -----//
$(function () {
    // script.type = 'text/javascript';
    var baseUrl = "faceMask";
    var srcList = [
        'router.js',
        'shopViewTmp.js',
        'mem.js',
        'order.js',
        'shopCart.js',
        'MemberController.js',
        'base.js'
    ];


    // var srcList = [
    //     'shopCart.js',
    //     'base.js'
    // ];

    srcList.forEach(function (data) {
        var script = document.createElement('script');
        script.src = baseUrl + "/" + data;  //要載入的js
        // script.src = data;  //要載入的js
        // $("head").append($("<script>").prop("src", data));
        $("head").append(script);

    });

    // nav.showPage(token_fun.get());
    window.location.hash = "#list";


    $(window).on('hashchange', function (e) {

        // var getP = window.location.search.substr(0);

        // console.log(document.location.hash);

        debugger;
        e.stopPropagation();
        e.preventDefault();
        // loadView(document.location.hash);
        nav.showPage(window.location.hash);
        return false;


    });
    if (window.location.hash) {
       $(window).trigger('hashchange');

    }


    //
    // (function () {
    //     $(window).on('hashchange', function (e) {
    //
    //         // var getP = window.location.search.substr(0);
    //
    //         // console.log(document.location.hash);
    //
    //         debugger;
    //         e.stopPropagation();
    //         e.preventDefault();
    //         // loadView(document.location.hash);
    //         nav.showPage(window.location.hash);
    //         return false;
    //
    //
    //     });
    // })();
    //
    // if (window.location.hash) {
    //    $(window).trigger('hashchange');
    //
    // }


});


// (function () {
//     $(window).on('hashchange', function () {
//         // var getP = window.location.search.substr(0);
//
//         // console.log(document.location.hash);
//
//         // loadView(document.location.hash);
//         nav.showPage(window.location.hash);
//
//
//     });
//     if (window.location.hash) {
//         $(window).trigger('hashchange');
//     }
// })();


// var load = (function () {
//     function loadJs() {
//
//         // alert(window.location.hash);
//
//         // script.type = 'text/javascript';
//         var baseUrl = "faceMask";
//         var srcList = [
//             'order.js',
//             'shopCart.js',
//             'MemberController.js',
//             'base.js'
//         ];
//
//
//         // var srcList = [
//         //     'shopCart.js',
//         //     'base.js'
//         // ];
//
//         srcList.forEach(function (data) {
//             var script = document.createElement('script');
//             script.src = baseUrl + "/" + data;  //要載入的js
//             // $("head").append($("<script>").prop("src", data));
//             $("head").append(script);
//
//         });
//     }
//
//     return {
//         loadJs: loadJs
//     }
// })();
//
// load.loadJs();