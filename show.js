var views = [

    {view: 'about.html', href: '#about'},
    {view: 'contact.html', href: '#contact'}

];

(function (window) {



    var $viewStage = $('#viewStage');

    function loadView(view) {

        switch (view) {
            case "about.html":

                var shop = new shopCart();
                shop.show();

                break;
            case "contact.html":
                $viewStage.html('<div class="loading"></div>').load(view);
                break;
        }


//            $viewStage.html('<div class="loading"></div>').load(view,function () {
////                alert(view);
////                $(this).find('#dd').click(function () {
////                    alert("dd");
////                })
//            });

    }

    /* matches the hash url to the view in the 'views' array of objects */
    function getView(hash) {
        for (var i = 0; i < views.length; i++) {
            if (views[i].href === hash) {
                return views[i].view;
                break;
            }
        }
    }

    /* listen for the anchor hashtag change */
    $(window).on('hashchange', function (e) {
//            alert(location.hash);
        loadView(getView(document.location.hash));

    });

    if (window.location.hash) {
        $(window).trigger('hashchange');
    }

})($(window));